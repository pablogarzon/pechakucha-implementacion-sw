## Presentacion del workflow de implementacion para la catedra de DSI de FRC UTN

### Uso:
- descargarlo
- descomprimirlo si descargaron el zip
- abrir la capreta
- abrir en el navegador el archivo index.html
- f11 para una presentación a pantalla completa

### Modificacion:
- editar el contenido de las etiquetas ```section ``` dentro de: ```<div class="slides">```

### comentarios:
para la presentación me base en el orden en que expone los temas este link: [Archivo PUD](http://www.ciens.ucv.ve:8080/genasig/sites/disist/archivos/clase8.pdf)

#### links utiles

[Reveal.js](https://github.com/hakimel/reveal.js)

[lenguaje markdown](https://www.markdowntutorial.com/)